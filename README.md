## Introduction
Welcome to the questions set. In this folder you'll find all the supporting files and the questions.  
To write answers or create answer files, you can either download this project (folder) to your PC, or you can `Fork` it using the button above and do your edits in the forked project.
When completed - you can send back the link to your forked project.

## Hints:
 - To execute SQL queries you may use a free cloud SQL service like http://sqlfiddle.com/. (from servers select `MS SQL Server`).
 - If you plan to use sqlfiddle then you already have a prebuilt schema on this link http://sqlfiddle.com/#!18/a00e59/2
 - To execute JS commands, you can use the browser's developer console, or free cloud solutions like codepen.io

## Scenario
*Data from the database is uploaded to an external app using the below query. Your questions will be related to this query:*
```sql
select ctv, POSID, stock, weekId from sample_sit;
```
*The `Data_upload_schema.json` is used to show to external app the column defininitions*

## Questions: 
[![Generic badge](https://img.shields.io/badge/SQL-green.svg)](https://shields.io/)
1. Update the above query to also include the `DirectCustomerId` and `Sellout` fields.
2. Update the query to also include the `WeekStartDay` from `sample_dates` table;
3. Add also the `AccountName` column to the query with the condition that if customer is missing, the data is still returned.
4. Show a small report that shows `Sellout` grouped by Customer (with customer name)
5. Given that in `ctv` column, the first 2 digits represent ScreenSize. Query a `Stock` report grouped by ScreenSize where stock is larger than 0.

[![Generic badge](https://img.shields.io/badge/JSON-orange.svg)](https://shields.io/)

 6. Update the `schema.json` file to match the fields queried by the above query?
 7. Is the schema an `object` or an `array` or an `array of objects`?

 [![Generic badge](https://img.shields.io/badge/JS-red.svg)](https://shields.io/)  

 8. Write a small script that will return array consisting of field names from `Data_upload_schema.json`

 9. What is the result of the following snippet;
 ```js
  import data from 'Data_upload_schema.json'
  console.log(data.objects.fields[3].type)
 ```

 10. What is the result of the following snippet:
  ```js
   const animals = [
     {name: 'cat', color: 'black', age: 3},
     {name: 'dog', color: 'red'},
     {name: 'turtle', color: 'green', age: 8}
   ]
   console.log(animals.cat.age)
   console.log(animals.dog.age)
   console.log(animals.dog?.age)
   console.log(animals.snake?.name)
  ```

 [![Generic badge](https://img.shields.io/badge/php-bonus_question-purple.svg)](https://shields.io/)  
 11. What is the result of the following code
 ```php
$strJsonFileContents = file_get_contents("Data_upload_schema.json");
echo count($strJsonFileContents->objects->fields);
 ```

